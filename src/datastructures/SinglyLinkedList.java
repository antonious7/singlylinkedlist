package datastructures;

/*Write a Singly linked list class and 3 methods: 
 *creation of the linked list, print all the elements, 
 *print all the elements in reverse order. 
 *Output elements should be comma separated.
 *-----------------------------------------
 *I assume it shouldn't be generic
 */

public class SinglyLinkedList {

	private Node head;
	private int size;
	public static final String SEPARATOR = ",";

	// LinkedList constructor
	public SinglyLinkedList() {
		head = new Node(null);
		size = 0;
	}

	// appends the specified element to the end of this list.
	public void add(Object data) {
		Node tempSLList = new Node(data);
		Node currSLList = head;
		// starting at the head node, crawl to the end of the list
		while (currSLList.getNext() != null) {
			currSLList = currSLList.getNext();
		}
		// the last node's "next" reference set to our new node
		currSLList.setNext(tempSLList);
		size++;// increment the number of elements variable
	}

	// inserts the specified element at the specified position in this list
	public void add(Object data, int index) {
		Node tempSLList = new Node(data);
		Node currSLList = head;
		// crawl to the requested index or the last element in the list,
		// whichever comes first
		for (int i = 1; i < index && currSLList.getNext() != null; i++) {
			currSLList = currSLList.getNext();
		}
		// set the new node's next-node reference to this node's next-node
		// reference
		tempSLList.setNext(currSLList.getNext());
		// now set this node's next-node reference to the new node
		currSLList.setNext(tempSLList);
		size++;// increment the number of elements variable
	}

	// returns the element at the specified position in this list.
	public Object get(int index) {
		// index must be 1 or higher
		if (index <= 0)
			return null;

		Node currSLList = head.getNext();
		for (int i = 1; i < index; i++) {
			if (currSLList.getNext() == null)
				return null;

			currSLList = currSLList.getNext();
		}
		return currSLList.getData();
	}

	// removes the element at the specified position in this list.
	public boolean remove(int index) {
		// if the index is out of range, exit
		if (index < 1 || index > size())
			return false;

		Node currSLList = head;
		for (int i = 1; i < index; i++) {
			if (currSLList.getNext() == null)
				return false;

			currSLList = currSLList.getNext();
		}
		currSLList.setNext(currSLList.getNext().getNext());
		size--; // decrement the number of elements variable
		return true;
	}

	// returns the number of elements in this list.
	public int size() {
		return size;
	}

	public String printAll() {
		Node currSLList = head.getNext();
		StringBuilder output = new StringBuilder();
		while (currSLList != null) {
			output.append(currSLList.getData());
			if(currSLList.next != null){
				output.append(SEPARATOR);
			}
			currSLList = currSLList.getNext();
		}
		return output.toString();
	}

	public String reverse(Node currSLList, StringBuilder output) {
		currSLList = currSLList.getNext();
		if (currSLList != null) {
			reverse(currSLList, output);
			if(currSLList.next != null) {
				output.append(SEPARATOR);
			}
			output.append(currSLList.getData().toString());
		}
		return output.toString();

	}

	public String printReversed() {
		StringBuilder output = new StringBuilder();
		return reverse(head, output);
		//return output.toString();
	}

	public String toString() {
		Node currSLList = head.getNext();
		String output = "";
		while (currSLList != null) {
			output += "[" + currSLList.getData().toString() + "]";
			currSLList = currSLList.getNext();
		}
		return output;
	}

	private class Node {
		/*
		 * @param Node reference to the next value
		 * @param data the data
		 */
		Node next;
		Object data;

		// Node constructor
		public Node(Object dataValue) {
			next = null;
			data = dataValue;
		}

		// another Node constructor if we want to
		// specify the node to point to.
		public Node(Object dataValue, Node nextValue) {
			next = nextValue;
			data = dataValue;
		}

		// these methods should be self-explanatory
		public Object getData() {
			return data;
		}

		public void setData(Object dataValue) {
			data = dataValue;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node nextValue) {
			next = nextValue;
		}
	}

	public static void main(String[] args) {
		SinglyLinkedList lList = new SinglyLinkedList();

		{
			System.out.println("Creating the Singly Linked List..");
		}
		// add elements to LinkedList
		lList.add("1");
		System.out.println("Adding value:" + lList.get(1));
		lList.add("2");
		System.out.println("Adding value:" + lList.get(2));
		lList.add("3");
		System.out.println("Adding value:" + lList.get(3));
		lList.add("4");
		System.out.println("Adding value:" + lList.get(4));
		lList.add("5");
		System.out.println("Adding value:" + lList.get(5));

		System.out.println("All elements are:");
		System.out.println(lList.printAll());
		System.out.println("All elements in the reversed order are:");
		System.out.println(lList.printReversed());
	}
}
